exports.command = function() {
    this.getText('css selector', 'h1', function(h1) {
        const h1Text = h1.value

        this.url(function(result){
            const savedUrl = result.value
    
            switch(true){
                case savedUrl.includes("/l/"):
                    break;
                    
                case h1Text === "Laundry":
                case h1Text === "Washing Machines":
                case h1Text === "Dishwashers":
                case h1Text === "Cooking":
                case h1Text === "Ovens":
                    this.click('.primaryButton')[0]
                    break;
                
                case h1Text === "Fridges & Freezers":
                case h1Text === "Fridge Freezers":
                    this.click('.ao-cta')[0]
                    break;
            }
        })
    })
    return this
}

