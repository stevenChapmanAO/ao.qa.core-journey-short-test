exports.command = function() {
        this
            .execute(function(){
                document.getElementsByClassName("addToBasket")[1].click()
            }).pause(2000) 

            this.elements('css selector', '.bottom-bar__go-to-basket-btn', result => {
                const numberOfAttachAddToBasket = result.value.length;        

                if(numberOfAttachAddToBasket > 0){
                    this.execute(function(){
                        document.getElementsByClassName("bottom-bar__go-to-basket-btn")[0].click()
                    })
                    this.assert.title("ao.com Shopping Basket")
                }
                else{
                    this.assert.title("ao.com Shopping Basket") 
                }
        })
        return this
    }

