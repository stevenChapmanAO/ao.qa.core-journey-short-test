# Core Journey Short (simple) smoke test [WORK IN PROGRESS]

## TODO:

- Add if environment is not prod then don't send the webhook
- Add support for teams fancy format cards
- Add percy demonstrator.

## What is this for?

This is for anyone interested in running some basic sanity on their changes across the UK, DE, Business and Boots Staging sites.

## Quick start:

### Install node (skip if you have it installed):

You can dowload node from [here](https://nodejs.org/en/). LTS should be fine, install and then go to your terminal and enter `node -v` if it gives you a version number then congrats you're good.

### Clone this repo and install dependencies:

Clone this repo, once down run the following in your terminal: `npm i`

### Run the test(s):

Run all tests in defined tests folder

`npm test`

Run specific test:

`npm t --test tests\filenameofyourtest.js`

## Logic, decision record and rationale

### NightwatchJS

**18/12/20:** I'm suggesting we use [NightwatchJS](https://nightwatchjs.org/) which is a Webdriver/Selenium based frame work like Webdriver IO. The logic behind choosing it is that it works well with BrowserStack (should we get to that point) and also is able to utilise testing library (not installed as of 18/12/20) which allows us to use a more Accessibility focused selector targeting strategy. Cypress was in my head but since it doens't play nice with the basket its just a bit of a no go for a holistic tool.

## Notes/Helpful links

### Commit messages

Commit messages, use [this pattern](http://karma-runner.github.io/0.10/dev/git-commit-msg.html) to make your commits.

### Adding new things

If you want to add another NPM package or tool make sure we all have a chat about it and understand (by all means experiment on your local branch at your discretion).

- If we all agree and you add in another npm package make sure to make it a dependency by adding in `--save-dev` in on the end e.g. `npm install nightwatch --save-dev`. That way who ever pulls it down will get the benefit when they `npm i`.
- Try not to push anything up thats not working locally for you so to limit impact on others.

### Environment Variables

Environment variables, using [this](https://www.npmjs.com/package/dotenv) should help. TLDR:

- `npm install dotenv --save-dev`
- Add the following: `require('dotenv').config()` to the relevant file at the top, (so for example the `nightwatch.conf.js`)
- Create a `.env` file in the root and the keys should follow this pattern `NAME=STEVE`
- Add your `.env` file to your `.gitignore` file so your private keys and so on aren't going up to the repo

## Goals

Provide a working example (boiler plate) of a Webdriver framework that can be used for cross browser continous deployment?

- Integration with cloud testing tools (Browserstack)
- Integration with image diffing tools (Percy?)
- Front end Performance testing?
- Accessibility testing integrations?
- Use of containerisation? (Docker)
- Use of AWS services (Secrets manager?)
