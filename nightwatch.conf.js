require("dotenv").config();
const chromedriver = require("chromedriver");
const postMessage = require("./teams-webhook");

module.exports = {
	src_folders: ["tests"],
	globals: {
		reporter: function (results, done) {
			postMessage(results);
			done();
		},
	},
	page_objects_path: "page_objects",
	custom_commands_path: "./commands",
	webdriver: {
		start_process: true,
		server_path: chromedriver.path,
		port: 9515,
	},
	test_settings: {
		default: {
			desiredCapabilities: {
				browserName: "chrome",
				chromeOptions: {
					args: ["headless", "no-sandbox", "disable-gpu"],
				},
			},
		},
	},
};
