function postMessage(results) {
	// This is the endpoint we are posting the message too, if things aren't working and you don't have a .env file locally with this environment variable then that might be your problem
	const url = process.env.INBOUND_TEAMS_WEBHOOK;

	// This is helping us with the last error. If there is no error then the property is not handled gracefully so we need to do this here by either returning the error or a string of our choosing
	let lastErrorPre = results.lastError
		? results.lastError
		: "No errors \u{1F60E}";
	let lastError = lastErrorPre.toString();

	// The NightwatchJS results object doesn't give us a pass or fail property. This works by checking for errors, if there is an error then we call it a fail.
	function getTestResult(errors) {
		if (errors < 1) {
			return "Passed \u{1F603}";
		} else {
			return "Failed \u{1F628}";
		}
	}

	// This is using the getTestResults function to determine if there has been a pass or fail and gives the JSON message something dynamic to work with.
	const errors = results.errors;
	const result = getTestResult(errors);

	fetch(url, {
		method: "POST",
		headers: {
			"Content-Type": "application/json",
		},
		body: JSON.stringify({
			"@type": "MessageCard",
			"@context": "http://schema.org/extensions",
			importance: "high", // not working right now
			themeColor: "0072C6",
			summary: "AO.QA.CORE-JOURNEY-SHORT-TEST",
			sections: [
				{
					activityTitle: "AO.QA.CORE-JOURNEY-SHORT-TEST Run Status",
					activitySubtitle: "Status: " + result,
					activityImage:
						"https://raw.githubusercontent.com/nightwatchjs/nightwatch/main/.github/assets/nightwatch-logo.png",
					facts: [
						{
							name: "Assertions Passed",
							value: results.passed,
						},
						{
							name: "Errors",
							value: results.errors,
						},
						{
							name: "Duration",
							value: results.elapsedTime + " seconds",
						},
						{
							name: "Last Error",
							value:
								"<pre style='white-space: pre-wrap;'>" + lastError + "</pre>",
							isSubtle: false,
							wrap: true,
						},
					],
					markdown: false,
				},
			],
		}),
	})
		.then((response) => {
			if (!response.ok) {
				throw new Error("Network response was not ok");
			}
			return response.json();
		})
		.then((data) => {
			console.log(data);
		})
		.catch((error) => {
			console.error("Error:", error);
		});
}

module.exports = postMessage;
